<?php

namespace Akempes\Mutations\Commands;

use Illuminate\Database\Console\Migrations\RollbackCommand;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\Migrations\Migrator;

class Rollback extends RollbackCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'mutate:rollback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback the last mutation';

    public function __construct()
    {
        $app = app();
        $repository = new DatabaseMigrationRepository($app['db'], config('mutations.database', 'mutations'));
        $migrator = new Migrator($repository, $app['db'], $app['files']);

        parent::__construct($migrator);
    }

    /**
     * Get the path to the migration directory.
     *
     * @return string
     */
    protected function getMigrationPath()
    {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'mutations';
    }
}
