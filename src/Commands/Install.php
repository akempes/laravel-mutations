<?php

namespace Akempes\Mutations\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\Console\Migrations\InstallCommand;

class Install extends InstallCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'mutate:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the mutations repository';

    public function __construct()
    {
        $app = app();
        $repository = new DatabaseMigrationRepository($app['db'], config('mutations.database', 'mutations'));

        parent::__construct($repository);
    }
}
