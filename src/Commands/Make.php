<?php

namespace Akempes\Mutations\Commands;

use Illuminate\Database\Console\Migrations\MigrateMakeCommand;
use Illuminate\Database\Migrations\MigrationCreator;

class Make extends MigrateMakeCommand
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'make:mutation {name : The name of the mutation class}
        {--create= : The table to be created}
        {--table= : The table to migrate}
        {--path= : The location where the mutation file should be created}
        {--realpath : Indicate any provided mutation file paths are pre-resolved absolute paths}
        {--fullpath : Output the full path of the mutation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new mutation file';

    public function __construct()
    {
        $app = app();
        $creator = new MigrationCreator($app['files'], __DIR__ . '/../stubs');

        parent::__construct($creator, $app['composer']);
    }

    /**
     * Get the path to the migration directory.
     *
     * @return string
     */
    protected function getMigrationPath()
    {
        return $this->laravel->databasePath() . DIRECTORY_SEPARATOR . 'mutations';
    }
}
