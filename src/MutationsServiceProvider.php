<?php

namespace Akempes\Mutations;

use Illuminate\Support\ServiceProvider;
use Akempes\Mutations\Commands\Install;
use Akempes\Mutations\Commands\Make;
use Akempes\Mutations\Commands\Rollback;
use Akempes\Mutations\Commands\Mutate;

class MutationsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__ . '/../config/Mutations.php' => config_path('Mutations.php')
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
                Make::class,
                Rollback::class,
                Mutate::class,
            ]);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
