# Laravel Mutations

Manage commands/code that should only run once like migrations. Ideal to perform mutations to your existing database during CI/CD pipelines.

# Installation
1. Install the package
```
php composer require akempes/laravel-mutations
```
2. Add the package service provider to `providers` section in your `./config/app.php` file.
```
'providers' => [
	...
	Akempes\Mutations\MutationsServiceProvider::class,
	...
],
```
3. (Optional) Publish the config file if you need to change the default table name 'mutations'.
```
php artisan vendor:publish --provider="Akempes\Mutations\MutationsServiceProvider"
```
4. Create the mutations repository
```
php artisan mutate:install
```

# How to use

Just like a migration, create a mutation:

```
php artisan make:mutation MyFirstMutation
```

The mutation files are created in the `database/mutations/` directory. Each mutation has an `up()` and `down()` method just like a migration.

To run your mutations:
```
php artisan mutate
```

And rollback:
```
php artisan mutate:rollback
```

